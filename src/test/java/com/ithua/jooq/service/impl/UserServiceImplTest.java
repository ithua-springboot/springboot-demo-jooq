package com.ithua.jooq.service.impl;

import com.ithua.jooq.JooqApplicationTests;
import com.ithua.jooq.generator.entity.tables.pojos.TUser;
import com.ithua.jooq.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class UserServiceImplTest extends JooqApplicationTests {
    @Autowired
    private UserService userService;

    @Test
    void queryByUsernameAndPassword() {
        System.out.println(
                userService.queryByUsernameAndPassword("wgh", "999999999")
        );
    }

    @Test
    void queryByUsername() {
        System.out.println(
                userService.queryByUsername("wgh")
        );
    }

    @Test
    public void insert() {
        System.out.println(
                userService.insert(new TUser()
                        .setUsername("wgh")
                        .setPassword("999999999")
                        .setPhone("18898656588")
                )
        );
    }

    @Test
    public void insertBatch() {
        List<TUser> users = new LinkedList<>(Arrays.asList(
                new TUser().setUsername("dd").setPassword("123456").setPhone("18898656500"),
                new TUser().setUsername("dd").setPassword("123456").setPhone("18898656500"))
        );
        System.out.println(
                userService.insertBatch(users)
        );
    }
}
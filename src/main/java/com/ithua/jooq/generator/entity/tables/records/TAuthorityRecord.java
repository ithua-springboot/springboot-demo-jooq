/*
 * This file is generated by jOOQ.
 */
package com.ithua.jooq.generator.entity.tables.records;


import com.ithua.jooq.generator.entity.tables.TAuthority;

import java.time.LocalDateTime;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record6;
import org.jooq.Row6;
import org.jooq.impl.UpdatableRecordImpl;
import org.jooq.types.ULong;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.11.11"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class TAuthorityRecord extends UpdatableRecordImpl<TAuthorityRecord> implements Record6<ULong, ULong, String, String, LocalDateTime, LocalDateTime> {

    private static final long serialVersionUID = -561951688;

    /**
     * Setter for <code>auth.t_authority.id</code>.
     */
    public void setId(ULong value) {
        set(0, value);
    }

    /**
     * Getter for <code>auth.t_authority.id</code>.
     */
    public ULong getId() {
        return (ULong) get(0);
    }

    /**
     * Setter for <code>auth.t_authority.parent_authority_id</code>.
     */
    public void setParentAuthorityId(ULong value) {
        set(1, value);
    }

    /**
     * Getter for <code>auth.t_authority.parent_authority_id</code>.
     */
    public ULong getParentAuthorityId() {
        return (ULong) get(1);
    }

    /**
     * Setter for <code>auth.t_authority.authority_name</code>.
     */
    public void setAuthorityName(String value) {
        set(2, value);
    }

    /**
     * Getter for <code>auth.t_authority.authority_name</code>.
     */
    public String getAuthorityName() {
        return (String) get(2);
    }

    /**
     * Setter for <code>auth.t_authority.description</code>.
     */
    public void setDescription(String value) {
        set(3, value);
    }

    /**
     * Getter for <code>auth.t_authority.description</code>.
     */
    public String getDescription() {
        return (String) get(3);
    }

    /**
     * Setter for <code>auth.t_authority.create_time</code>.
     */
    public void setCreateTime(LocalDateTime value) {
        set(4, value);
    }

    /**
     * Getter for <code>auth.t_authority.create_time</code>.
     */
    public LocalDateTime getCreateTime() {
        return (LocalDateTime) get(4);
    }

    /**
     * Setter for <code>auth.t_authority.update_time</code>.
     */
    public void setUpdateTime(LocalDateTime value) {
        set(5, value);
    }

    /**
     * Getter for <code>auth.t_authority.update_time</code>.
     */
    public LocalDateTime getUpdateTime() {
        return (LocalDateTime) get(5);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Record1<ULong> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record6 type implementation
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Row6<ULong, ULong, String, String, LocalDateTime, LocalDateTime> fieldsRow() {
        return (Row6) super.fieldsRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Row6<ULong, ULong, String, String, LocalDateTime, LocalDateTime> valuesRow() {
        return (Row6) super.valuesRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<ULong> field1() {
        return TAuthority.T_AUTHORITY.ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<ULong> field2() {
        return TAuthority.T_AUTHORITY.PARENT_AUTHORITY_ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field3() {
        return TAuthority.T_AUTHORITY.AUTHORITY_NAME;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field4() {
        return TAuthority.T_AUTHORITY.DESCRIPTION;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<LocalDateTime> field5() {
        return TAuthority.T_AUTHORITY.CREATE_TIME;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<LocalDateTime> field6() {
        return TAuthority.T_AUTHORITY.UPDATE_TIME;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ULong component1() {
        return getId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ULong component2() {
        return getParentAuthorityId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component3() {
        return getAuthorityName();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component4() {
        return getDescription();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public LocalDateTime component5() {
        return getCreateTime();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public LocalDateTime component6() {
        return getUpdateTime();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ULong value1() {
        return getId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ULong value2() {
        return getParentAuthorityId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value3() {
        return getAuthorityName();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value4() {
        return getDescription();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public LocalDateTime value5() {
        return getCreateTime();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public LocalDateTime value6() {
        return getUpdateTime();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TAuthorityRecord value1(ULong value) {
        setId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TAuthorityRecord value2(ULong value) {
        setParentAuthorityId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TAuthorityRecord value3(String value) {
        setAuthorityName(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TAuthorityRecord value4(String value) {
        setDescription(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TAuthorityRecord value5(LocalDateTime value) {
        setCreateTime(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TAuthorityRecord value6(LocalDateTime value) {
        setUpdateTime(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TAuthorityRecord values(ULong value1, ULong value2, String value3, String value4, LocalDateTime value5, LocalDateTime value6) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        value5(value5);
        value6(value6);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached TAuthorityRecord
     */
    public TAuthorityRecord() {
        super(TAuthority.T_AUTHORITY);
    }

    /**
     * Create a detached, initialised TAuthorityRecord
     */
    public TAuthorityRecord(ULong id, ULong parentAuthorityId, String authorityName, String description, LocalDateTime createTime, LocalDateTime updateTime) {
        super(TAuthority.T_AUTHORITY);

        set(0, id);
        set(1, parentAuthorityId);
        set(2, authorityName);
        set(3, description);
        set(4, createTime);
        set(5, updateTime);
    }
}

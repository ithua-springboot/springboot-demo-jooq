/*
 * This file is generated by jOOQ.
 */
package com.ithua.jooq.generator.entity.tables;


import com.ithua.jooq.generator.entity.Auth;
import com.ithua.jooq.generator.entity.Indexes;
import com.ithua.jooq.generator.entity.Keys;
import com.ithua.jooq.generator.entity.tables.records.TGroupUserRecord;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Identity;
import org.jooq.Index;
import org.jooq.Name;
import org.jooq.Record;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.TableImpl;
import org.jooq.types.ULong;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.11.11"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class TGroupUser extends TableImpl<TGroupUserRecord> {

    private static final long serialVersionUID = -73559101;

    /**
     * The reference instance of <code>auth.t_group_user</code>
     */
    public static final TGroupUser T_GROUP_USER = new TGroupUser();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<TGroupUserRecord> getRecordType() {
        return TGroupUserRecord.class;
    }

    /**
     * The column <code>auth.t_group_user.id</code>.
     */
    public final TableField<TGroupUserRecord, ULong> ID = createField("id", org.jooq.impl.SQLDataType.BIGINTUNSIGNED.nullable(false).identity(true), this, "");

    /**
     * The column <code>auth.t_group_user.group_id</code>.
     */
    public final TableField<TGroupUserRecord, ULong> GROUP_ID = createField("group_id", org.jooq.impl.SQLDataType.BIGINTUNSIGNED.nullable(false), this, "");

    /**
     * The column <code>auth.t_group_user.user_id</code>.
     */
    public final TableField<TGroupUserRecord, ULong> USER_ID = createField("user_id", org.jooq.impl.SQLDataType.BIGINTUNSIGNED.nullable(false), this, "");

    /**
     * The column <code>auth.t_group_user.create_time</code>.
     */
    public final TableField<TGroupUserRecord, LocalDateTime> CREATE_TIME = createField("create_time", org.jooq.impl.SQLDataType.LOCALDATETIME.nullable(false), this, "");

    /**
     * The column <code>auth.t_group_user.update_time</code>.
     */
    public final TableField<TGroupUserRecord, LocalDateTime> UPDATE_TIME = createField("update_time", org.jooq.impl.SQLDataType.LOCALDATETIME.nullable(false), this, "");

    /**
     * Create a <code>auth.t_group_user</code> table reference
     */
    public TGroupUser() {
        this(DSL.name("t_group_user"), null);
    }

    /**
     * Create an aliased <code>auth.t_group_user</code> table reference
     */
    public TGroupUser(String alias) {
        this(DSL.name(alias), T_GROUP_USER);
    }

    /**
     * Create an aliased <code>auth.t_group_user</code> table reference
     */
    public TGroupUser(Name alias) {
        this(alias, T_GROUP_USER);
    }

    private TGroupUser(Name alias, Table<TGroupUserRecord> aliased) {
        this(alias, aliased, null);
    }

    private TGroupUser(Name alias, Table<TGroupUserRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""));
    }

    public <O extends Record> TGroupUser(Table<O> child, ForeignKey<O, TGroupUserRecord> key) {
        super(child, key, T_GROUP_USER);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Schema getSchema() {
        return Auth.AUTH;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Index> getIndexes() {
        return Arrays.<Index>asList(Indexes.T_GROUP_USER_GROUP_ID_IDX, Indexes.T_GROUP_USER_PRIMARY, Indexes.T_GROUP_USER_USER_ID_IDX);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Identity<TGroupUserRecord, ULong> getIdentity() {
        return Keys.IDENTITY_T_GROUP_USER;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UniqueKey<TGroupUserRecord> getPrimaryKey() {
        return Keys.KEY_T_GROUP_USER_PRIMARY;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UniqueKey<TGroupUserRecord>> getKeys() {
        return Arrays.<UniqueKey<TGroupUserRecord>>asList(Keys.KEY_T_GROUP_USER_PRIMARY);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TGroupUser as(String alias) {
        return new TGroupUser(DSL.name(alias), this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TGroupUser as(Name alias) {
        return new TGroupUser(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public TGroupUser rename(String name) {
        return new TGroupUser(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public TGroupUser rename(Name name) {
        return new TGroupUser(name, null);
    }
}

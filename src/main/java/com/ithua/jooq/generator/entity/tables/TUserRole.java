/*
 * This file is generated by jOOQ.
 */
package com.ithua.jooq.generator.entity.tables;


import com.ithua.jooq.generator.entity.Auth;
import com.ithua.jooq.generator.entity.Indexes;
import com.ithua.jooq.generator.entity.Keys;
import com.ithua.jooq.generator.entity.tables.records.TUserRoleRecord;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Identity;
import org.jooq.Index;
import org.jooq.Name;
import org.jooq.Record;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.TableImpl;
import org.jooq.types.ULong;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.11.11"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class TUserRole extends TableImpl<TUserRoleRecord> {

    private static final long serialVersionUID = -1632983610;

    /**
     * The reference instance of <code>auth.t_user_role</code>
     */
    public static final TUserRole T_USER_ROLE = new TUserRole();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<TUserRoleRecord> getRecordType() {
        return TUserRoleRecord.class;
    }

    /**
     * The column <code>auth.t_user_role.id</code>.
     */
    public final TableField<TUserRoleRecord, ULong> ID = createField("id", org.jooq.impl.SQLDataType.BIGINTUNSIGNED.nullable(false).identity(true), this, "");

    /**
     * The column <code>auth.t_user_role.user_id</code>.
     */
    public final TableField<TUserRoleRecord, ULong> USER_ID = createField("user_id", org.jooq.impl.SQLDataType.BIGINTUNSIGNED.nullable(false), this, "");

    /**
     * The column <code>auth.t_user_role.role_id</code>.
     */
    public final TableField<TUserRoleRecord, ULong> ROLE_ID = createField("role_id", org.jooq.impl.SQLDataType.BIGINTUNSIGNED.nullable(false), this, "");

    /**
     * The column <code>auth.t_user_role.create_time</code>.
     */
    public final TableField<TUserRoleRecord, LocalDateTime> CREATE_TIME = createField("create_time", org.jooq.impl.SQLDataType.LOCALDATETIME.nullable(false), this, "");

    /**
     * The column <code>auth.t_user_role.update_time</code>.
     */
    public final TableField<TUserRoleRecord, LocalDateTime> UPDATE_TIME = createField("update_time", org.jooq.impl.SQLDataType.LOCALDATETIME.nullable(false), this, "");

    /**
     * Create a <code>auth.t_user_role</code> table reference
     */
    public TUserRole() {
        this(DSL.name("t_user_role"), null);
    }

    /**
     * Create an aliased <code>auth.t_user_role</code> table reference
     */
    public TUserRole(String alias) {
        this(DSL.name(alias), T_USER_ROLE);
    }

    /**
     * Create an aliased <code>auth.t_user_role</code> table reference
     */
    public TUserRole(Name alias) {
        this(alias, T_USER_ROLE);
    }

    private TUserRole(Name alias, Table<TUserRoleRecord> aliased) {
        this(alias, aliased, null);
    }

    private TUserRole(Name alias, Table<TUserRoleRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""));
    }

    public <O extends Record> TUserRole(Table<O> child, ForeignKey<O, TUserRoleRecord> key) {
        super(child, key, T_USER_ROLE);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Schema getSchema() {
        return Auth.AUTH;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Index> getIndexes() {
        return Arrays.<Index>asList(Indexes.T_USER_ROLE_PRIMARY, Indexes.T_USER_ROLE_ROLE_ID_IDX, Indexes.T_USER_ROLE_USER_ID_IDX);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Identity<TUserRoleRecord, ULong> getIdentity() {
        return Keys.IDENTITY_T_USER_ROLE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UniqueKey<TUserRoleRecord> getPrimaryKey() {
        return Keys.KEY_T_USER_ROLE_PRIMARY;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UniqueKey<TUserRoleRecord>> getKeys() {
        return Arrays.<UniqueKey<TUserRoleRecord>>asList(Keys.KEY_T_USER_ROLE_PRIMARY);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TUserRole as(String alias) {
        return new TUserRole(DSL.name(alias), this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TUserRole as(Name alias) {
        return new TUserRole(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public TUserRole rename(String name) {
        return new TUserRole(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public TUserRole rename(Name name) {
        return new TUserRole(name, null);
    }
}

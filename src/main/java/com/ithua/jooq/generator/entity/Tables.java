/*
 * This file is generated by jOOQ.
 */
package com.ithua.jooq.generator.entity;


import com.ithua.jooq.generator.entity.tables.TAuthority;
import com.ithua.jooq.generator.entity.tables.TGroup;
import com.ithua.jooq.generator.entity.tables.TGroupRole;
import com.ithua.jooq.generator.entity.tables.TGroupUser;
import com.ithua.jooq.generator.entity.tables.TRole;
import com.ithua.jooq.generator.entity.tables.TRoleAuthority;
import com.ithua.jooq.generator.entity.tables.TUser;
import com.ithua.jooq.generator.entity.tables.TUserRole;

import javax.annotation.Generated;


/**
 * Convenience access to all tables in auth
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.11.11"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Tables {

    /**
     * The table <code>auth.t_authority</code>.
     */
    public static final TAuthority T_AUTHORITY = com.ithua.jooq.generator.entity.tables.TAuthority.T_AUTHORITY;

    /**
     * The table <code>auth.t_group</code>.
     */
    public static final TGroup T_GROUP = com.ithua.jooq.generator.entity.tables.TGroup.T_GROUP;

    /**
     * The table <code>auth.t_group_role</code>.
     */
    public static final TGroupRole T_GROUP_ROLE = com.ithua.jooq.generator.entity.tables.TGroupRole.T_GROUP_ROLE;

    /**
     * The table <code>auth.t_group_user</code>.
     */
    public static final TGroupUser T_GROUP_USER = com.ithua.jooq.generator.entity.tables.TGroupUser.T_GROUP_USER;

    /**
     * The table <code>auth.t_role</code>.
     */
    public static final TRole T_ROLE = com.ithua.jooq.generator.entity.tables.TRole.T_ROLE;

    /**
     * The table <code>auth.t_role_authority</code>.
     */
    public static final TRoleAuthority T_ROLE_AUTHORITY = com.ithua.jooq.generator.entity.tables.TRoleAuthority.T_ROLE_AUTHORITY;

    /**
     * The table <code>auth.t_user</code>.
     */
    public static final TUser T_USER = com.ithua.jooq.generator.entity.tables.TUser.T_USER;

    /**
     * The table <code>auth.t_user_role</code>.
     */
    public static final TUserRole T_USER_ROLE = com.ithua.jooq.generator.entity.tables.TUserRole.T_USER_ROLE;
}

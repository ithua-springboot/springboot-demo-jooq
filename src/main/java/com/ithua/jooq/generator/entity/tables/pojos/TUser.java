/*
 * This file is generated by jOOQ.
 */
package com.ithua.jooq.generator.entity.tables.pojos;


import lombok.Data;
import lombok.experimental.Accessors;
import org.jooq.types.ULong;

import javax.annotation.Generated;
import java.io.Serializable;
import java.time.LocalDateTime;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.11.11"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
@Data
@Accessors(chain = true)
public class TUser implements Serializable {

    private static final long serialVersionUID = 215684455;

    private ULong         id;
    private String        username;
    private String        password;
    private String        phone;
    private LocalDateTime createTime = LocalDateTime.now();
    private LocalDateTime updateTime = LocalDateTime.now();
}

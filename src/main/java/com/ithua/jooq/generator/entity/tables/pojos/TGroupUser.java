/*
 * This file is generated by jOOQ.
 */
package com.ithua.jooq.generator.entity.tables.pojos;


import java.io.Serializable;
import java.time.LocalDateTime;

import javax.annotation.Generated;

import org.jooq.types.ULong;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.11.11"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class TGroupUser implements Serializable {

    private static final long serialVersionUID = 252341110;

    private ULong         id;
    private ULong         groupId;
    private ULong         userId;
    private LocalDateTime createTime;
    private LocalDateTime updateTime;

    public TGroupUser() {}

    public TGroupUser(TGroupUser value) {
        this.id = value.id;
        this.groupId = value.groupId;
        this.userId = value.userId;
        this.createTime = value.createTime;
        this.updateTime = value.updateTime;
    }

    public TGroupUser(
        ULong         id,
        ULong         groupId,
        ULong         userId,
        LocalDateTime createTime,
        LocalDateTime updateTime
    ) {
        this.id = id;
        this.groupId = groupId;
        this.userId = userId;
        this.createTime = createTime;
        this.updateTime = updateTime;
    }

    public ULong getId() {
        return this.id;
    }

    public void setId(ULong id) {
        this.id = id;
    }

    public ULong getGroupId() {
        return this.groupId;
    }

    public void setGroupId(ULong groupId) {
        this.groupId = groupId;
    }

    public ULong getUserId() {
        return this.userId;
    }

    public void setUserId(ULong userId) {
        this.userId = userId;
    }

    public LocalDateTime getCreateTime() {
        return this.createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return this.updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }
}

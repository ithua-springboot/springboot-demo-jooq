package com.ithua.jooq.generator;

import org.apache.commons.lang3.StringUtils;
import org.jooq.codegen.GenerationTool;
import org.jooq.codegen.JavaGenerator;
import org.jooq.meta.jaxb.Configuration;
import org.jooq.meta.jaxb.Database;
import org.jooq.meta.jaxb.Generate;
import org.jooq.meta.jaxb.Generator;
import org.jooq.meta.jaxb.Jdbc;
import org.jooq.meta.jaxb.Target;

public class JOOQGenerator extends JavaGenerator {
    // jdbc
    private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    private static final String JDBC_URL = "jdbc:mysql://localhost:3306";
    private static final String JDBC_USERNAME = "root";
    private static final String JDBC_PASSWORD = "999999999";

    private static final String JOOQ_DATABASE_NAME = "org.jooq.meta.mysql.MySQLDatabase";

    private static final String DIRECTORY = "src/main/java";
    private static final String PACKAGE_NAME = "com.ithua.jooq.generator.entity";
    private static final String DATABASE_NAME  = "auth";
    private static final String INCLUDES = ".*";
    private static final String EXCLUDES = "";

    public static void main(String[] args) throws Exception {
        generateDefaultTables();
    }

    private static void generateDefaultTables() throws Exception {
        generatorTables(DATABASE_NAME, PACKAGE_NAME, INCLUDES, EXCLUDES);
    }

    public static void generatorTables(String schemaName, String packageName, String includes, String excludes) throws Exception {
        Jdbc jdbc = createJdbc();
        Generator generator = createGenerator(schemaName, packageName, includes, excludes);
        Configuration configuration = new Configuration()
                .withJdbc(jdbc)
                .withGenerator(generator);
        GenerationTool.generate(configuration);
    }

    private static Jdbc createJdbc() {
        return new Jdbc()
                .withDriver(JDBC_DRIVER)
                .withUrl(JDBC_URL)
                .withUser(JDBC_USERNAME)
                .withPassword(JDBC_PASSWORD);
    }

    private static Generator createGenerator(String schemaName, String packageName, String includes, String excludes) {
        Database database = createDatabase(schemaName, includes, excludes);
        Target target = createTarget(packageName);
        Generate generate = createGenerate();
        return new Generator()
                .withDatabase(database)
                .withTarget(target)
                .withGenerate(generate);
    }

    private static Database createDatabase(String schemaName, String includes, String excludes) {
        Database database = new Database()
                .withName(JOOQ_DATABASE_NAME)
                .withInputSchema(schemaName)
                .withSyntheticPrimaryKeys("public\\..*\\.id")
                .withOverridePrimaryKeys("override_primmary_key");
        if (StringUtils.isNotBlank(includes)) {
            database.withIncludes(includes);
        }

        if (StringUtils.isNotBlank(excludes)) {
            database.withExcludes(excludes);
        }
        return database;
    }

    private static Target createTarget(String packageName) {
        return new Target()
                .withDirectory(DIRECTORY)
                .withPackageName(packageName);
    }

    private static Generate createGenerate() {
        return new Generate()
                .withPojos(true)
                .withJavaTimeTypes(true)
                // .withJavaBeansGettersAndSetters(false)
                .withPojosEqualsAndHashCode(true)
                .withCommentsOnAttributes(true);
    }

    // Configuration configuration = new Configuration()
    //                 .withGenerator(new Generator()
    //                         .withDatabase(new Database()
    //                                 .withName("org.jooq.meta.extensions.ddl.DDLDatabase")
    //                                 .withProperties(
    //                                         new Property()
    //                                                 .withKey("scripts")
    //                                                 .withValue("src/main/resources/database.sql"),
    //                                         new Property()
    //                                                 .withKey("sort")
    //                                                 .withValue("semantic")))));
    //         GenerationTool.generate(configuration);
}


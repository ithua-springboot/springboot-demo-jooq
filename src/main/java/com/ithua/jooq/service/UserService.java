package com.ithua.jooq.service;

import com.ithua.jooq.generator.entity.tables.pojos.TUser;

import java.util.List;

public interface UserService {
    TUser queryByUsernameAndPassword(String username, String password);

    TUser queryByUsername(String username);

    int insert(TUser user);

    int insertBatch(List<TUser> users);
}

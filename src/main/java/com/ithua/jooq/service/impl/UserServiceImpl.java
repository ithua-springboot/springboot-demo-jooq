package com.ithua.jooq.service.impl;

import com.ithua.jooq.generator.entity.tables.pojos.TUser;
import com.ithua.jooq.service.UserService;
import lombok.RequiredArgsConstructor;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Service("userService")
public class UserServiceImpl implements UserService {
    private final DSLContext create;

    com.ithua.jooq.generator.entity.tables.TUser u = com.ithua.jooq.generator.entity.tables.TUser.T_USER.as("u");

    @Override
    public TUser queryByUsernameAndPassword(String username, String password) {
        return create.selectFrom(u)
                .where(u.USERNAME.eq(username))
                .and(u.PASSWORD.eq(password))
                .fetchOneInto(TUser.class);
    }

    @Override
    public TUser queryByUsername(String username) {
        return create.selectFrom(u)
                .where(u.USERNAME.eq(username))
                .fetchAnyInto(TUser.class);
    }

    @Override
    public int insert(TUser user) {
        return create.newRecord(u, user).insert();
    }

    @Override
    public int insertBatch(List<TUser> users) {
        // return create.batchInsert(users);
        return 0;
    }
}
